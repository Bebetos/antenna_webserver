
class PageCreator:

    def getLinesFromFile(self):
        fd = open('results.txt', 'r')
        return fd.readlines()

    def createHTMLPage(self):

        fd = open('index.html', 'w')
        fd.write("<html>")
        fd.write("<head><title>Pagina risultati</title></head>")
        fd.write("<body>")
        fd.write("<table><tr><th>ID</th><th>Campo</th><th>Valore</th></tr>")

        lines = self.getLinesFromFile()

        for line in lines:
            values = line.split(";")
            fd.write("<tr>")

            for value in values:
                fd.write("<td>" + value + "</td>")

            fd.write("</tr>")

        fd.write("</table></body></html>")

    def getHTMLPage(self):

        fd = open("index.html", "r")
        return fd.read()

